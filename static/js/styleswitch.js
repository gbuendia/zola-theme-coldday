function getFromCookie() {
    var cookies = document.cookie;
    // https://stackoverflow.com/a/25490531
    return document.cookie.match('(^|;)\\s*buendia_style\\s*=\\s*([^;]+)')?.pop() || '';
}

function getFromTag(element) {
    // We get the href attribute, split it at the slashes and get the last bit:
    var filename = element.href.split("/").at(-1);
    return "/" + filename;
}

function switchStyles() {
    // We get the full <link> element:
    var style = document.getElementById("styles");

    var current = getFromCookie();

    if (current == "") {
        var current = getFromTag(style);
    }

    if (current == "/warm.css") {
        var changeTo = "/cold.css";
    } else if (current == "/cold.css") {
        var changeTo = "/warm.css";
    } else {
        console.log("Error trying to switch stylesheet");
    }
    
    style.setAttribute("href", changeTo);
    document.cookie = "buendia_style=" + changeTo + "; sameSite=Strict; path=/";
  }